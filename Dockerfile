FROM node:6.11.1
MAINTAINER JEONGHWAN KIM <soo_bak@sk.com>
RUN mkdir -p /app
WORKDIR /app
RUN mkdir /root/.ssh
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
RUN git clone https://gitlab.com/soobak12/vehicles-manager.git /app
RUN npm install
ENV NODE_ENV development
EXPOSE 3000 80
CMD ["node","./bin/www"]
