'use strict';
module.exports = (sequelize, DataTypes) => {
  const vehicleMST = sequelize.define('vehicleMST', {
    vehicleCode: DataTypes.STRING,
    vehicleName: DataTypes.STRING,
    groupCode: DataTypes.STRING,
    fuelCode: DataTypes.STRING,
    bodyCode: DataTypes.STRING,
    apprCode: DataTypes.STRING,
    colorCode: DataTypes.STRING,
    price: DataTypes.INTEGER,
    releaseDate: DataTypes.STRING,
    displacement: DataTypes.INTEGER,
    driveKind: DataTypes.STRING,
    capacity: DataTypes.INTEGER,
    regrId: DataTypes.STRING,
    updrId: DataTypes.STRING
  }, {});
  vehicleMST.associate = function(models) {
    // associations can be defined here
  };
  return vehicleMST;
};