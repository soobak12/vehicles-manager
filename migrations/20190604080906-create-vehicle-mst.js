'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('vehicleMSTs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      vehicleCode: {
        type: Sequelize.STRING
      },
      vehicleName: {
        type: Sequelize.STRING
      },
      groupCode: {
        type: Sequelize.STRING
      },
      fuelCode: {
        type: Sequelize.STRING
      },
      bodyCode: {
        type: Sequelize.STRING
      },
      apprCode: {
        type: Sequelize.STRING
      },
      colorCode: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.INTEGER
      },
      releaseDate: {
        type: Sequelize.STRING
      },
      displacement: {
        type: Sequelize.INTEGER
      },
      driveKind: {
        type: Sequelize.STRING
      },
      capacity: {
        type: Sequelize.INTEGER
      },
      regrId: {
        type: Sequelize.STRING
      },
      updrId: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('vehicleMSTs');
  }
};