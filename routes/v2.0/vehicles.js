var express = require('express');
var router = express.Router();
var model = require('../models/index');

/* GET todo listing. */
router.get('/all', function(req, res, next) {
  model.vehicleMST.findAll({})
   .then(vehicles => res.json({
      error: false,
      data: vehicles
    }))
    .catch(error => res.json({
      error: true,
      data: [],
      error: error
    }));
});

/* POST todo. */
router.post('/create', function(req, res, next) {
  const {
    vehicleCode,
    vehicleName,
    groupCode,
    fuelCode,
    bodyCode,
    apprCode,
    colorCode,
    price,
    releaseDate,
    displacement,
    driveKind,
    capacity,
    regrId,
    updrId
  } = req.body;
  model.vehicleMST.create({
    vehicleCode: vehicleCode,
    vehicleName: vehicleName,
    groupCode: groupCode,
    fuelCode: fuelCode,
    bodyCode: bodyCode,
    apprCode: apprCode,
    colorCode: colorCode,
    price: price,
    releaseDate: releaseDate,
    displacement: displacement,
    driveKind: driveKind,
    capacity: capacity,
    regrId: regrId,
    updrId: updrId
    })
    .then(vehicle => res.status(201).json({
        error: false,
        data: vehicle,
        message: 'New vehicle has been created.'
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        error: error
    }));

});


/* update todo. */
router.put('/update/:input', function(req, res, next) {
  const inputCode = req.params.input;
  const {
    vehicleCode,
    vehicleName,
    groupCode,
    fuelCode,
    bodyCode,
    apprCode,
    colorCode,
    price,
    releaseDate,
    displacement,
    driveKind,
    capacity,
    regrId,
    updrId
  } = req.body;
  model.vehicleMST.update({
    vehicleCode: vehicleCode,
    vehicleName: vehicleName,
    groupCode: groupCode,
    fuelCode: fuelCode,
    bodyCode: bodyCode,
    apprCode: apprCode,
    colorCode: colorCode,
    price: price,
    releaseDate: releaseDate,
    displacement: displacement,
    driveKind: driveKind,
    capacity: capacity,
    regrId: regrId,
    updrId: updrId
     }, {
         where: {
             vehicleCode: inputCode
         }
     })
     .then(vehicle => res.json({
         error: false,
         message: 'vehicle has been updated.'
     }))
     .catch(error => res.json({
         error: true,
         error: error
     }));
});


router.delete('/delete/:input', function(req, res, next) {
    const inputCode = req.params.input;
    model.vehicleMST.destroy({ where: {
        vehicleCode: inputCode
    }})
        .then(status => res.json({
            error: false,
            message: 'vehicle has been delete.'
        }))
        .catch(error => res.json({
            error: true,
            error: error
        }));
});

module.exports = router;
